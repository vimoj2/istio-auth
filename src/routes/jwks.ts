import express from "express";
import { pem2jwk } from "pem-jwk";
import { readFileSync } from "fs";
const router = express.Router();

const publicKey = readFileSync("public.pem");

router.get("/.well-known/jwks.json", (req, res) => {
  res.json({
    keys: [pem2jwk(publicKey.toString())],
  });
});

export { router as jwksRouter };
