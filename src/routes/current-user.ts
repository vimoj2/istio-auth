import express from "express";
import jwt from "jsonwebtoken";
const router = express.Router();

router.get("/api/users/currentUser", (req, res) => {
  // !req.session || !req.session.jwt = !req.session?.jwt
  if (!req.session?.jwt) {
    res.send({ currentUser: null });
  }

  try {
    const payload = jwt.verify(req.session?.jwt, process.env.JWT_KEY!);
    res.send({ currentUser: payload });
  } catch (error) {
    console.log("Error in jwt verification login: ", error);
    res.send({ currentUser: null });
  }
});

export { router as currentUserRouter };
