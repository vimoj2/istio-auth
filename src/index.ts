import express from "express";
import mongoose from "mongoose";
import "express-async-errors";
import { json } from "body-parser";
import cookieSession from "cookie-session";

import { currentUserRouter } from "./routes/current-user";
import { signinRouter } from "./routes/signin";
import { signoutRouter } from "./routes/signout";
import { signupRouter } from "./routes/signup";
import { jwksRouter } from "./routes/jwks";

import { errorHandler } from "./middlewares/error-handler";
import { NotFoundError } from "./errors/not-found-error";
const { MONGO_HOST, MONGO_DB, MONGO_USER, MONGO_PASSWORD } = process.env;

const app = express();
app.set("trust proxy", true);

app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: true,
  })
);

app.use(currentUserRouter);
app.use(signinRouter);
app.use(signoutRouter);
app.use(signupRouter);
app.use(jwksRouter);

app.all("*", async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

const start = async () => {
  try {
    await mongoose.connect(`mongodb://${MONGO_HOST}:27017`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      dbName: MONGO_DB,
      user: MONGO_USER,
      pass: MONGO_PASSWORD,
    });
    console.log("Connected to MongoDB");
  } catch (error) {
    console.log("Db connection error: ", error);
  }
};

app.listen(3000, () => {
  console.log("listening on port 3000");
});

start();
